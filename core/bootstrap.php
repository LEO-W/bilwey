<?php

require "controllers/PagesController.php";
require "core/Router.php";
require "core/Request.php";

function page($data) {
    return require "pages/".$data.".php";
}